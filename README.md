# Detect Spam Email in Spanish

En este proyecto se busca crear un lista de palabras que son constantes en correos que fueron
enviados a SPAM, se tomarón 200 archivos .eml.

A partir de estos correos se crea un CSV en el cual se guardan esa lista de palabras.

Previamente en el codigo SpamDetect.py lee ese archivo CSV, luego lee los correos en el directorio seleccionado
se separan en dos grupos de correos entrenamiento 80% y 20% prueba el cual se le dará al cliente. 

En este proyecto falta como pruebas correos que no fueron a SPAM y estos mismos deberian ser agregados a la lista de prueba para verificar si los detecta o no como SPAM.

El método para detectar el SPAM fue Naive Bayes, el cual clasifica por pesos de probabilidad si un correo en función
de las palabras que se encuentren será lanzando como SPAM o no. 

# Trabajos a futuro (08/06/23)

El archivo readText.py no lee correctamente los archivos de correo, esto se debe a que la lectura viene en formto HTML 
el cual toca parsear, luego omitir links e imagenes. 

Esta es una corrección obligatoria que debe implementarse en ambos archivos para leer correctamente los archivos.eml
