#Libreria para trabajar con los emails.
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.naive_bayes import MultinomialNB

#Librerias para leer email y convertir a string

import os
import email
from bs4 import BeautifulSoup
import html2text


def main():

    # Se supone que aquí leería mi excel en el cual se encontrarían las 30 palabras más usadas en correos de spam
    spam_keywords = ['dinero', 'aprovecha', 'gana', 'oferta', 'descuento', 'compra', 'gratis', 'urgente', 'premio','oportunidad']


    # Aquí irían algunos de los correos, entre el 75% y el 80%, lo que se haría es leer el contenido de los mismo con el algoritmo de leer el email
    # Problema que presenta, está detectando tanto links como imagenes, esto debe implementarse para se obviado
    readEmails = readEmail()
    #Nos aseguramos de tener el 80% de los datos para entrenamiento
    only80percent = int(len(readEmails)*0.8)
    # trainin_emails = readEmails[:only80percent]
    # se utiliza un conjunto de entrenamiento falso 
    training_emails = [
        "Gana dinero rápido y fácil, usando nuestra plataforma en oferta, obten maravillosos descuentos",
        "¡Suscríbete ahora y obtén grandes descuentos!, es Urgente, no puedes dejar pasar esta oportunidad, gana un premio de dinero por más",
        "Descubre el mejor préstamo para ti y tu familia no lo dejes pasar",
        "Oferta limitada: compra dos y lleva uno gratis, en caso de no recibir este descuento no ganarás"
    ]

    training_labels = np.array([1, 1, 0, 0])

    vectorizer = CountVectorizer(vocabulary=spam_keywords)
    vectorized_emails = vectorizer.fit_transform(training_emails)

    classifier = MultinomialNB()
    classifier.fit(vectorized_emails, training_labels)

    #Aquí solo queremos el porcentaje restante de los datos para probrar nuestra aplicación
    only20Percent = int(len(readEmails)*0.2)
    # test_emails = readEmails[only20Percent:]
    test_emails = [
        "Aprovecha esta oportunidad única",
        "Gana un premio increíble",
        "Descubre la mejor manera de ahorrar"
    ]

    vectorized_test_emails = vectorizer.transform(test_emails)
    predictions = classifier.predict(vectorized_test_emails)

    #Hacemos un for para dectar en el conjunto de prueba cuales pueden o no ser spam
    for i in range(len(test_emails)):
        print(f"Correo: {test_emails[i]}")
        print(f"Es spam: {'si' if predictions[i] == 1 else 'No'}")
        print()
#Funcion para obtener el str de los correos. está detectando links que generan problemas.
def readEmail():
    arrayReturn = []
    directory = "/home/omar/Documentos/Documentos/Universidad/IA/Proyecto IA/emails/Prueba"
    for file in os.listdir(directory):
        if file.endswith(".eml"):
            print(file)
            ruta_archivo = os.path.join(directory, file)
            with open(ruta_archivo, "r") as f:
                # Parsear el archivo .eml utilizando la biblioteca email
                msg = email.message_from_file(f)
                # Extraer el contenido de texto del correo electrónico
                content = ""
                for part in msg.walk():
                    if part.get_content_type() == "text/html":
                        htmldata=part.get_payload(decode=True).decode('utf-8')
                        converteDoc= html2text.html2text(htmldata)  
                        arrayReturn.append(converteDoc) 
    return  arrayReturn

main()